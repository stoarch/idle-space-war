unit EnemyShip;

interface

uses
  System.Classes,
  FMX.Objects;

type
  TEnemyStatus = (esUnknown, esAlive, esDead);

type
  TEnemyHitEvent = procedure (Sender: TObject; Amount: Integer) of object;

type
  // [ TEnemyShip ]=================================
  // Goal:
  // Provide life management of enemy and fire events when it hurt or
  // destroyed
  //
  TEnemyShip = class
  private
    FMaxHP: Integer;
    FCurrentHP: Integer;
    FEnemyStatus: TEnemyStatus;
    FOnHit: TEnemyHitEvent;
    FOnDeath: TNotifyEvent;
    FPrice: Currency;
    FView : TRectangle;

    procedure SetCurrentHP(const Value: Integer);
    procedure SetMaxHP(const Value: Integer);
    function GetEnemyStatus: TEnemyStatus;
    procedure SetOnDeath(const Value: TNotifyEvent);
    procedure SetOnHit(const Value: TEnemyHitEvent);
    procedure FireOnHit(amount: integer);
    procedure FireOnDeath;
    procedure SetPrice(const Value: Currency);
  public
    constructor Create(aMaxHP: integer; anView: TRectangle);

    // Methods //
    procedure Hit( amount: integer );
    procedure Kill;
    procedure ResetHP;

    // Properties //
    property CurrentHP: Integer read FCurrentHP write SetCurrentHP;
    property MaxHP: Integer read FMaxHP write SetMaxHP;

    property Status: TEnemyStatus read GetEnemyStatus;
    property Price: Currency read FPrice write SetPrice;

    property View : TRectangle read FView;

    // Events //
    property OnHit: TEnemyHitEvent read FOnHit write SetOnHit;
    property OnDeath: TNotifyEvent read FOnDeath write SetOnDeath;
  end;

implementation

{ TEnemyShip }

constructor TEnemyShip.Create(aMaxHP: integer; anView : TRectangle);
begin
  FMaxHP := aMaxHP;
  FCurrentHP := aMaxHP;
  FEnemyStatus := esAlive;
  FView := anView;
end;

function TEnemyShip.GetEnemyStatus: TEnemyStatus;
begin
  Result := FEnemyStatus;
end;

procedure TEnemyShip.Hit(amount: integer);
begin
  CurrentHP := CurrentHP - amount;

  FireOnHit(amount);

  if CurrentHP <= 0 then
  begin
    Kill;
  end;
end;

procedure TEnemyShip.FireOnHit( amount: integer );
begin
  if Assigned(FOnHit) then
  begin
    FOnHit(self, amount);
  end;
end;

procedure TEnemyShip.FireOnDeath;
begin
  if Assigned(FOnDeath) then
  begin
    FOnDeath(self);
  end;
end;

procedure TEnemyShip.Kill;
begin
  FEnemyStatus := esDead;
  FireOnDeath;
end;

procedure TEnemyShip.ResetHP;
begin
  FCurrentHP := MaxHP;
  FEnemyStatus := esAlive;
end;

procedure TEnemyShip.SetCurrentHP(const Value: Integer);
begin
  FCurrentHP := Value;
end;

procedure TEnemyShip.SetMaxHP(const Value: Integer);
begin
  FMaxHP := Value;
end;

procedure TEnemyShip.SetOnDeath(const Value: TNotifyEvent);
begin
  FOnDeath := Value;
end;

procedure TEnemyShip.SetOnHit(const Value: TEnemyHitEvent);
begin
  FOnHit := Value;
end;

procedure TEnemyShip.SetPrice(const Value: Currency);
begin
  FPrice := Value;
end;

end.
