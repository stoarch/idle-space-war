unit bullet;

interface

uses
  System.Classes, System.Math, System.Math.Vectors, System.Types,
  FMX.Objects,
  enemyShip;

type
  TBulletHitEvent = procedure(sender: TObject; anEnemy: TEnemyShip) of object;

  TBullet = class
  private
    FDamage: Integer;
    FTravelSpeed: real;
    FTravelDirection: TVector;
    FOnBulletExpires: TNotifyEvent;
    FOnHitEnemy: TBulletHitEvent;
    FView: TRectangle;
    FActive: Boolean;

    function CanHitEnemy(anEnemy: TEnemyShip): boolean;
    procedure HitEnemy(anEnemy: TEnemyShip);
    procedure SetDamage(const Value: Integer);
    procedure SetOnBulletExpires(const Value: TNotifyEvent);
    procedure SetOnHitEnemy(const Value: TBulletHitEvent);
    procedure SetTravelDirection(const Value: TVector);
    procedure SetTravelSpeed(const Value: real);
    procedure SetActive(const Value: Boolean);
  public
    constructor Create(view: TRectangle); reintroduce; overload;


    // methods //

    procedure CheckHitEnemy(anEnemy: TEnemyShip);
    procedure Update;

    // properties //
    property Active: Boolean read FActive write SetActive;

    property TravelDirection: TVector read FTravelDirection
      write SetTravelDirection;
    property TravelSpeed: real read FTravelSpeed write SetTravelSpeed;

    property Damage: Integer read FDamage write SetDamage;

    property View: TRectangle read FView;

    // events //
    property OnHitEnemy: TBulletHitEvent read FOnHitEnemy write SetOnHitEnemy;

    // This event tells manage class that this bullet can be freed or
    // hidden into pool for future use
    property OnBulletExpires: TNotifyEvent read FOnBulletExpires
      write SetOnBulletExpires;
  end;

implementation

{ TBullet }

function TBullet.CanHitEnemy(anEnemy: TEnemyShip): boolean;
begin
  result := IntersectRect( FView.BoundsRect, anEnemy.View.BoundsRect );
end;

procedure TBullet.CheckHitEnemy(anEnemy: TEnemyShip);
begin
  if CanHitEnemy(anEnemy) then
    HitEnemy(anEnemy);
end;

constructor TBullet.Create(view: TRectangle);
begin
  FView := view;
end;

procedure TBullet.HitEnemy(anEnemy: TEnemyShip);
begin
  Assert(anEnemy <> nil, 'Enemy should be set');

  anEnemy.Hit(FDamage);

  if assigned(FOnHitEnemy) then
    FOnHitEnemy(self, anEnemy);

end;

procedure TBullet.SetActive(const Value: Boolean);
begin
  FActive := Value;
end;

procedure TBullet.SetDamage(const Value: integer);
begin
  FDamage := Value;
end;

procedure TBullet.SetOnBulletExpires(const Value: TNotifyEvent);
begin
  FOnBulletExpires := Value;
end;

procedure TBullet.SetOnHitEnemy(const Value: TBulletHitEvent);
begin
  FOnHitEnemy := Value;
end;

procedure TBullet.SetTravelDirection(const Value: TVector);
begin
  FTravelDirection := Value.Normalize;
end;

procedure TBullet.SetTravelSpeed(const Value: real);
begin
  FTravelSpeed := Value;
end;

procedure TBullet.Update;
begin
  if not active then
    exit;

  FView.Position.Point := PointF(
    FView.Position.X + FTravelSpeed*FTravelDirection.X,
    FView.Position.Y + FTravelSpeed*FTravelDirection.Y);
end;

end.
