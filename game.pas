unit game;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Platform,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Objects, FMX.Ani, FMX.Effects,
  enemyShip, FMX.Filter.Effects, System.Math, FMX.Layouts,
  System.Generics.Collections, System.Math.Vectors,
  Bullet;

type
  TIdleSpaceWar = class(TForm)
    CurrentEnemyRectangle: TRectangle;
    EnemyHitFX: TRoundRect;
    PlayerShip: TRectangle;
    TickTimer: TTimer;
    EnemyLabel: TLabel;
    EnemyHPLabel: TLabel;
    EnemyHPProgressBar: TProgressBar;
    DamageFlyoutLabel: TLabel;
    DamageFlyoutPathAnimation: TPathAnimation;
    ProgressBar1: TProgressBar;
    Label1: TLabel;
    waveEnemyCountLabel: TLabel;
    EnemyGlowEffect: TGlowEffect;
    PlayerLaserLine: TLine;
    DissolveTransitionEffect: TDissolveTransitionEffect;
    dissolveProgressFloatAnimation: TFloatAnimation;
    flyInPositionXFloatAnimation: TFloatAnimation;
    CoinsValueLabel: TLabel;
    MoneyCircle: TCircle;
    MoneyPath1: TPath;
    MoneyPathAnimation1: TPathAnimation;
    PlayerDamageCaptionLabel: TLabel;
    PlayerDamageLabel: TLabel;
    UpgradePlayerDamageButton: TCornerButton;
    UpgradeDamagePrice: TLabel;
    UpgradeDamageValueLabel: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PlayerLevelLabel: TLabel;
    BossTimer: TTimer;
    AllyCannonShipRectangle: TRectangle;
    AssetsLayout: TLayout;
    BulletRectangle5: TRectangle;
    BulletRectangle1: TRectangle;
    BulletRectangle2: TRectangle;
    BulletRectangle3: TRectangle;
    BulletRectangle4: TRectangle;
    AllyShootTimer: TTimer;
    AllyUpgradeGroupbox: TGroupBox;
    UpgradeAllyDamageButton: TCornerButton;
    HireAllySupportButton: TCornerButton;
    UpgradeAllyFirerateButton: TCornerButton;
    UpgradeRatio1RadioButton: TRadioButton;
    UpgradeRatio10RadioButton: TRadioButton;
    UpgradeRatio100RadioButton: TRadioButton;
    UpgradeRatioMaxRadioButton: TRadioButton;
    AllyArriveFloatAnimation: TFloatAnimation;
    CoinsLabel: TLabel;
    HireAllyPriceLabelel: TLabel;
    ShowAllyUpgradeGroupBoxFloatAnimation: TFloatAnimation;
    AllyDamageLabel: TLabel;
    AllyFireRateLabel: TLabel;
    UpgradeAllyPriceLabel: TLabel;
    AllyLevelLabel: TLabel;
    procedure TickTimerTimer(Sender: TObject);
    procedure CurrentEnemyRectangleClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DamageFlyoutPathAnimationFinish(Sender: TObject);
    procedure DamageFlyoutPathAnimationProcess(Sender: TObject);
    procedure dissolveProgressFloatAnimationFinish(Sender: TObject);
    procedure flyInPositionXFloatAnimationFinish(Sender: TObject);
    procedure MoneyPathAnimation1Finish(Sender: TObject);
    procedure UpgradePlayerDamageButtonClick(Sender: TObject);
    procedure AllyShootTimerTimer(Sender: TObject);
    procedure HireAllySupportButtonClick(Sender: TObject);
    procedure AllyArriveFloatAnimationFinish(Sender: TObject);
    procedure UpgradeRatio1RadioButtonChange(Sender: TObject);
    procedure UpgradeRatio10RadioButtonChange(Sender: TObject);
    procedure UpgradeRatio100RadioButtonChange(Sender: TObject);
    procedure UpgradeRatioMaxRadioButtonChange(Sender: TObject);
    procedure UpgradeAllyDamageButtonClick(Sender: TObject);
    procedure UpgradeAllyFirerateButtonClick(Sender: TObject);
  private
    ActiveDamageFlyoutLabel: Boolean;
    CurrentEnemyHP: Integer;
    MaxEnemyHP: Integer;
    HitFXTimeout: Integer;
    FEnemyShip: TEnemyShip;
    FCoins: Currency;
    FCurrentRound: Integer;
    FMaxRounds: Integer;
    FPlayerPower: Integer;
    FUpgradeDamagePrice: Currency;
    FPlayerPowerUpgrade: Integer;
    FPlayerLevel: Integer;
    FEnemyDestroying: Boolean;
    FBulletPool : TQueue<TBullet>;
    FActiveBulletList : TList<TBullet>;
    FHireAllyPrice: Currency;
    FUpgradeAllyPrice: Currency;
    FAllyLevel: integer;
    FAllyFirerate: real;
    FAllyDamage: integer;
    FUpgradeAllyFactor: integer; //increase in levels (1..100..max)

    procedure UpdateGameState;
    procedure HandleEnemyShipHit(Sender: TObject; Amount: Integer);
    procedure HandleEnemyShipDeath(Sender: TObject);
    procedure SetEnemyHPView;
    procedure GainMoney(Amount: Currency);
    procedure UpdateCoinsValueView;
    procedure StartNewRound;
    procedure UpdateRoundView;
    procedure CheckUpgradeButtons;
    procedure UpgradeDamage;
    procedure UpdateDamagePriceView;
    procedure UpdateUpgradeDamagePrice;
    procedure UpdatePlayerPowerUpgrade;
    procedure UpdatePlayerDamageView;
    procedure UpdateDamageUpgradeView;
    procedure UpdatePlayerLevelView;
    procedure IncreasePlayerPower;
    procedure IncreasePlayerLevel;
    procedure SpendCoinsOnUpgrade;
    procedure UpdateEnemyMaxHP;
    procedure PrepareBulletPool;
    function CreateBullet(anView: TRectangle): TBullet;
    function GetPooledBullet: TBullet;
    procedure ShootBulletAtEnemy;
    procedure ActivateBullet(bullet: TBullet);
    procedure PlaceBulletAtAlly(abullet: TBullet);
    procedure FreeBulletQueue;
    procedure FreeActiveBulletList;
    procedure LogError(message: String);
    procedure SetBulletDirection(abullet: TBullet);
    procedure HandleBulletHitEnemy(sender: TObject; anEnemy: TEnemyShip);
    procedure HitEnemy(currentDamage: Integer);
    procedure ExpireBullet(abullet: TBullet);
    procedure HideAllAssets;
    procedure HireAllySupport;
    procedure SetCoins(const Value: Currency);
    procedure SetHireAllyPrice(const Value: Currency);
    procedure UpdateAllyPriceView;
    procedure CheckHireAllyButton;
    procedure CheckAllyUpgradeButtons;
    procedure SetUpgradeAllyPriceForFactor(afactor: integer);
    procedure UpdateUpgradeAllyPriceView;
    procedure SetAllyDamage(const Value: integer);
    procedure SetAllyFireRate(const Value: real);
    procedure SetAllyLevel(const Value: integer);

    property Coins: Currency read FCoins write SetCoins;
    property HireAllyPrice: Currency read FHireAllyPrice write SetHireAllyPrice;
    property AllyLevel: integer read FAllyLevel write SetAllyLevel;
    property AllyDamage: integer read FAllyDamage write SetAllyDamage;
    property AllyFireRate: real read FAllyFireRate write SetAllyFireRate;
  public
    destructor Destroy; override;
    { Public declarations }
  end;

var
  IdleSpaceWar: TIdleSpaceWar;

implementation

const
  damageFlyoutDx: Real = 3;
  damageFlyoutDy: Real = -3;
  endDamageFlyoutPoint: TPointF = (
    x: 600;
    y: 50
  );
  startDamageFlyoutPoint: TPointF = (
    x: 544;
    y: 96
  );

procedure RenderingSetupCallback(const Sender, Context: TObject; var ColorBits, DepthBits: Integer; var Stencil: Boolean; var Multisamples: Integer);
begin
  // Override OpenGL rendering setup to use custom values.
  ColorBits := 16; // default is 24
  DepthBits := 0; // default is 24
  Stencil := False; // default is True
  Multisamples := 0; // default depends on TForm.Quality or TForm3D.Multisample
end;

procedure RegisterRenderingSetup;
var
  SetupService: IFMXRenderingSetupService;
begin
  if TPlatformServices.Current.SupportsPlatformService(IFMXRenderingSetupService, IInterface(SetupService)) then
    SetupService.Subscribe(RenderingSetupCallback);
  // There is also SetupService.Unsubscribe, which removes the hook.
end;

{$R *.fmx}

procedure TIdleSpaceWar.TickTimerTimer(Sender: TObject);
begin
  UpdateGameState;
end;

procedure TIdleSpaceWar.CurrentEnemyRectangleClick(Sender: TObject);
begin
  if FEnemyDestroying then
    Exit;

  HitEnemy(FPlayerPower);
end;

procedure TIdleSpaceWar.DamageFlyoutPathAnimationFinish(Sender: TObject);
begin
  ActiveDamageFlyoutLabel := False;
  DamageFlyoutLabel.Visible := False;
end;

procedure TIdleSpaceWar.DamageFlyoutPathAnimationProcess(Sender: TObject);
begin
  EnemyLabel.Text := Format('%4.2f:%4.2f', [DamageFlyoutLabel.Position.X, DamageFlyoutLabel.Position.Y]);
end;

procedure TIdleSpaceWar.HitEnemy(currentDamage: Integer);
begin
  FEnemyShip.Hit(currentDamage);

  GainMoney(currentDamage);

  if CurrentEnemyHP <= 0 then
  begin

  end;
end;

procedure TIdleSpaceWar.FormCreate(Sender: TObject);
begin
  CurrentEnemyHP := 30;
  MaxEnemyHP := 30;

  DamageFlyoutPathAnimation.Path.MoveTo(PointF(0, 0));
  DamageFlyoutPathAnimation.Path.LineTo(PointF(40, -40));

  FEnemyShip := TEnemyShip.Create(MaxEnemyHP, CurrentEnemyRectangle);
  FEnemyShip.Price := 10; //TODO: Make config with enemy waves: HP/Price and boss HP/timeout/Loot/Price

  FEnemyShip.OnHit := HandleEnemyShipHit;
  FEnemyShip.OnDeath := HandleEnemyShipDeath;

  FCurrentRound := 1;
  FMaxRounds := 10;

  FPlayerPower := 1;
  FPlayerLevel := 0;

  FUpgradeDamagePrice := 3; //10*n + 3
  FPlayerPowerUpgrade := 1; //2*n + 1

  HideAllAssets;
  AllyUpgradeGroupbox.Visible := false;

  PrepareBulletPool;

  HireAllyPrice := 250; //TODO: move to config for easier setup by game designer

  FAllyLevel := 0;
  FAllyFirerate := 0.2;
  FAllyDamage := 2;
  FUpgradeAllyPrice := 150;
  FUpgradeAllyFactor := 1;
end;

procedure TIdleSpaceWar.HideAllAssets;
begin
  for var asset in  AssetsLayout.Children do
    if asset is TControl then
    with asset as TControl do
    begin
      Visible := false;
    end;
end;

procedure TIdleSpaceWar.HireAllySupportButtonClick(Sender: TObject);
begin
  HireAllySupport;
end;

procedure TIdleSpaceWar.HireAllySupport();
begin
  Coins := Coins - FHireAllyPrice;
  AllyCannonShipRectangle.Visible := true;
  AllyArriveFloatAnimation.Enabled := true;
  HireAllySupportButton.Visible := false;
  HireAllyPriceLabelel.Visible := false;
end;

procedure TIdleSpaceWar.PrepareBulletPool();
begin
  FBulletPool := TQueue<TBullet>.Create(); //here we manage them (inactive)
  FActiveBulletList := TList<TBullet>.Create(); //here we store them (active one)

  //TODO: Make bullet search with RTTI
  FBulletPool.Enqueue(CreateBullet( BulletRectangle1 ));
  FBulletPool.Enqueue(CreateBullet( BulletRectangle2 ));
  FBulletPool.Enqueue(CreateBullet( BulletRectangle3 ));
  FBulletPool.Enqueue(CreateBullet( BulletRectangle4 ));
  FBulletPool.Enqueue(CreateBullet( BulletRectangle5 ));
end;

function TIdleSpaceWar.CreateBullet( anView: TRectangle ) : TBullet;
begin
  result := TBullet.Create(anView);
  result.Active := false; //by default bullet is inactive and invisible
  result.Damage := 2;
//TODO: Get it from table (yaml/csv/json)
  result.OnHitEnemy := HandleBulletHitEnemy;
end;

procedure TIdleSpaceWar.HandleBulletHitEnemy(sender: TObject; anEnemy: TEnemyShip);
var
  bullet: TBullet;
begin
  if not (sender is TBullet) then
  begin
    LogError('Bullet hit should provide bullet sender');
    exit;
  end;

  bullet := sender as TBullet;

  HitEnemy(bullet.Damage);
  ExpireBullet(bullet);
end;

procedure TIdleSpaceWar.ExpireBullet(abullet: TBullet);
begin
  FActiveBulletList.Remove(abullet);

  abullet.Active := false;
  abullet.view.Visible := false;

  FBulletPool.Enqueue(abullet);
end;

destructor TIdleSpaceWar.Destroy;
begin
  if Assigned(FEnemyShip) then
  begin
    FreeAndNil(FEnemyShip);
  end;

  FreeActiveBulletList;
  FreeAndNil( FActiveBulletList );

  FreeBulletQueue;
  FreeAndNil( FBulletPool );

  inherited;
end;

procedure TIdleSpaceWar.FreeActiveBulletList;
begin
  for var i := 0 to FActiveBulletList.Count - 1 do
  begin
    var bullet := FActiveBulletList[i];

    FreeAndNil( bullet );
    FActiveBulletList[i] := nil;
  end;
end;

procedure TIdleSpaceWar.FreeBulletQueue;
  var
    bullet: TBullet;
begin
  while FBulletPool.Count > 0  do
  begin
    bullet := FBulletPool.Dequeue;
    FreeAndNil( bullet );
  end;
end;

procedure TIdleSpaceWar.UpdateEnemyMaxHP;
begin
  FEnemyShip.MaxHP := Trunc(Power(FCurrentRound,2)) * 7 + 25;
  FEnemyShip.ResetHP;
  SetEnemyHPView;
end;

procedure TIdleSpaceWar.SpendCoinsOnUpgrade;
begin
  FCoins := FCoins - FUpgradeDamagePrice;
end;

procedure TIdleSpaceWar.IncreasePlayerLevel;
begin
  Inc(FPlayerLevel);
end;

procedure TIdleSpaceWar.IncreasePlayerPower;
begin
  Inc(FPlayerPower, FPlayerPowerUpgrade);
end;

procedure TIdleSpaceWar.UpdatePlayerLevelView;
begin
  PlayerLevelLabel.Text := IntToStr(FPlayerLevel);
end;

procedure TIdleSpaceWar.UpdateDamageUpgradeView;
begin
  UpgradeDamageValueLabel.Text := 'Damage: +' + IntToStr(FPlayerPowerUpgrade);
end;

procedure TIdleSpaceWar.UpdatePlayerDamageView;
begin
  PlayerDamageLabel.Text := IntToStr(FPlayerPower);
end;

procedure TIdleSpaceWar.UpdatePlayerPowerUpgrade;
begin
  FPlayerPowerUpgrade := FPlayerLevel * 2 + 1;
end;

procedure TIdleSpaceWar.UpdateUpgradeDamagePrice;
begin
  FUpgradeDamagePrice := Trunc(Power(FPlayerLevel,3)) + 3;
end;

procedure TIdleSpaceWar.UpdateDamagePriceView;
begin
  UpgradeDamagePrice.Text := Format('Price: %m', [FUpgradeDamagePrice]);
end;

procedure TIdleSpaceWar.UpdateRoundView;
begin
  waveEnemyCountLabel.Text := Format('Enemy %d/%d', [FCurrentRound, FMaxRounds]);
end;

procedure TIdleSpaceWar.UpgradePlayerDamageButtonClick(Sender: TObject);
begin
  UpgradeDamage;
end;

procedure TIdleSpaceWar.UpgradeRatio100RadioButtonChange(Sender: TObject);
begin
  SetUpgradeAllyPriceForFactor(100);
end;

procedure TIdleSpaceWar.UpgradeRatio10RadioButtonChange(Sender: TObject);
begin
  SetUpgradeAllyPriceForFactor(10);
end;

procedure TIdleSpaceWar.UpgradeRatio1RadioButtonChange(Sender: TObject);
begin
  SetUpgradeAllyPriceForFactor(1);
end;

procedure TIdleSpaceWar.UpgradeRatioMaxRadioButtonChange(Sender: TObject);
  var
    newFactor: integer;
begin
  newFactor := Ceil(Power(FCoins - 5,0.25)) - FAllyLevel;
  SetUpgradeAllyPriceForFactor(newFactor);
end;

procedure TIdleSpaceWar.SetUpgradeAllyPriceForFactor( afactor: integer );
begin
  if afactor < 0 then
    afactor := 1;

  FUpgradeAllyPrice := IntPower(FAllyLevel + afactor, 4) + 149;
  FUpgradeAllyFactor := afactor;

  UpdateUpgradeAllyPriceView;
end;

procedure TIdleSpaceWar.UpdateUpgradeAllyPriceView;
begin
  UpgradeAllyPriceLabel.Text := Format('Price: %m', [FUpgradeAllyPrice]);
end;

procedure TIdleSpaceWar.UpgradeAllyDamageButtonClick(Sender: TObject);
begin
  Coins := Coins - FUpgradeAllyPrice;
  AllyDamage := AllyDamage + AllyLevel*FUpgradeAllyFactor;
  AllyLevel := AllyLevel + FUpgradeAllyFactor;
  SetUpgradeAllyPriceForFactor(1);
end;

procedure TIdleSpaceWar.UpgradeAllyFirerateButtonClick(Sender: TObject);
begin
  Coins := Coins - FUpgradeAllyPrice;
  AllyLevel := AllyLevel + FUpgradeAllyFactor;
  AllyFireRate := 5/(Log2(AllyLevel) + 1);
  SetUpgradeAllyPriceForFactor(1);
end;

procedure TIdleSpaceWar.UpgradeDamage();
begin
  SpendCoinsOnUpgrade;

  IncreasePlayerPower;
  IncreasePlayerLevel;

  UpdatePlayerPowerUpgrade;
  UpdateUpgradeDamagePrice;

  UpdateDamagePriceView;
  UpdatePlayerDamageView;
  UpdateDamageUpgradeView;
  UpdateCoinsValueView;
  UpdatePlayerLevelView;

  CheckUpgradeButtons;
end;

procedure TIdleSpaceWar.StartNewRound;
begin
  Inc(FCurrentRound);

  UpdateRoundView;
  UpdateEnemyMaxHP;

  FEnemyDestroying := false;

  if FCurrentRound > FMaxRounds then
  begin
    //TODO: Make boss appears
  end;
end;

procedure TIdleSpaceWar.UpdateCoinsValueView;
begin
  CoinsValueLabel.Text := CurrToStr(FCoins);
end;

procedure TIdleSpaceWar.SetEnemyHPView;
begin
  EnemyHPLabel.Text := Format('%d/%d', [FEnemyShip.CurrentHP, FEnemyShip.MaxHP]);
  EnemyHPProgressBar.Value := FEnemyShip.CurrentHP;
end;

procedure TIdleSpaceWar.SetHireAllyPrice(const Value: Currency);
begin
  FHireAllyPrice := Value;
  UpdateAllyPriceView;
end;

procedure TIdleSpaceWar.UpdateAllyPriceView();
begin
  HireAllyPriceLabelel.Text := Format('Price: %m', [FHireAllyPrice]);
end;

procedure TIdleSpaceWar.dissolveProgressFloatAnimationFinish(Sender: TObject);
begin
  DissolveTransitionEffect.Enabled := false;
  flyInPositionXFloatAnimation.Start;
end;

procedure TIdleSpaceWar.flyInPositionXFloatAnimationFinish(Sender: TObject);
begin
  StartNewRound;
end;

procedure TIdleSpaceWar.GainMoney(Amount: Currency);
begin
  FCoins := FCoins + Amount;
  UpdateCoinsValueView;

  MoneyCircle.Position := CurrentEnemyRectangle.Position;
  MoneyCircle.Visible := true;

  CheckUpgradeButtons;
  CheckHireAllyButton;
  CheckAllyUpgradeButtons;
end;

procedure TIdleSpaceWar.CheckAllyUpgradeButtons();
begin
  UpgradeAllyDamageButton.Enabled := FCoins > FUpgradeAllyPrice;
  UpgradeAllyFirerateButton.Enabled := FCoins > FUpgradeAllyPrice;
end;

procedure TIdleSpaceWar.CheckHireAllyButton;
begin
  if FCoins > FHireAllyPrice then
  begin
    HireAllySupportButton.Enabled := true;
  end;
end;

procedure TIdleSpaceWar.AllyArriveFloatAnimationFinish(Sender: TObject);
begin
  AllyShootTimer.Enabled := true;
  AllyUpgradeGroupbox.Visible := true;
  ShowAllyUpgradeGroupBoxFloatAnimation.Enabled := true;
end;

procedure TIdleSpaceWar.AllyShootTimerTimer(Sender: TObject);
begin
  ShootBulletAtEnemy;
end;

procedure TIdleSpaceWar.ShootBulletAtEnemy;
  var
    bullet: TBullet;
begin
  bullet := GetPooledBullet();
  if bullet = nil then
  begin
    //LogError('Bullet can not be received. Pool is empty');
    exit;
  end;

  PlaceBulletAtAlly(bullet);
  SetBulletDirection(bullet);
  ActivateBullet(bullet);
end;

procedure TIdleSpaceWar.LogError( message: String );
  const
    FILE_NAME = 'isw.log';
  var
    f: Text;
begin
  AssignFile( f, FILE_NAME );
  if FileExists(FILE_NAME) then
    Append( f )
  else
    Rewrite(f);

  WriteLn( f, Format('%s - %s', [FormatDateTime('hh:nn:ss',Now), message]));
  Flush(f);
  CloseFile(f);
end;

procedure TIdleSpaceWar.PlaceBulletAtAlly( abullet: TBullet );
begin
  abullet.View.Position.Point :=
     AllyCannonShipRectangle.Position.Point + PointF(50,20) ;
end;

procedure TIdleSpaceWar.SetAllyDamage(const Value: integer);
begin
  FAllyDamage := Value;
  AllyDamageLabel.Text := Format('Damage: %d',[Value]);
end;

procedure TIdleSpaceWar.SetAllyFireRate(const Value: real);
begin
  FAllyFireRate := Value;
  AllyFireRateLabel.Text := Format('Firerate: %.2f sec', [FAllyFireRate]);
  AllyShootTimer.Interval := Round(5000/FAllyFireRate);
end;

procedure TIdleSpaceWar.SetAllyLevel(const Value: integer);
begin
  FAllyLevel := Value;
  AllyLevelLabel.Text := Format('Level: %d', [FAllyLevel]);
end;

procedure TIdleSpaceWar.SetBulletDirection( abullet: TBullet );
begin
  abullet.TravelDirection := Vector( CurrentEnemyRectangle.Position.Point - AllyCannonShipRectangle.Position.Point );
  abullet.TravelSpeed := 2;
end;

procedure TIdleSpaceWar.SetCoins(const Value: Currency);
begin
  FCoins := Value;
  UpdateCoinsValueView;
end;

procedure TIdleSpaceWar.ActivateBullet( bullet: TBullet );
begin
  FActiveBulletList.Add(bullet); //we need to place it because this list updates all of active one
  bullet.Active := true;
  bullet.View.Visible := true;
end;

function TIdleSpaceWar.GetPooledBullet() : TBullet;
begin
  result := nil;
  if FBulletPool.Count = 0 then
  begin
    exit;
  end;

  result := FBulletPool.Dequeue;
end;

procedure TIdleSpaceWar.CheckUpgradeButtons;
begin
  UpgradePlayerDamageButton.Enabled := FCoins >= FUpgradeDamagePrice;
end;

procedure TIdleSpaceWar.HandleEnemyShipHit(Sender: TObject; Amount: Integer);
begin
  ActiveDamageFlyoutLabel := true;

  DamageFlyoutLabel.Position.SetPointNoChange(startDamageFlyoutPoint);
  DamageFlyoutLabel.Visible := true;
  DamageFlyoutLabel.Text := IntToStr(Amount);

  DamageFlyoutPathAnimation.Start;

  EnemyGlowEffect.Enabled := true;
  PlayerLaserLine.Visible := true;

  SetEnemyHPView;

  EnemyHitFX.Visible := true;

  HitFXTimeout := 1;
end;

procedure TIdleSpaceWar.HandleEnemyShipDeath(Sender: TObject);
begin
  FEnemyDestroying := True;

  GainMoney(FEnemyShip.Price);

  DissolveTransitionEffect.Progress := 0;
  DissolveTransitionEffect.Enabled := True;
  dissolveProgressFloatAnimation.Start;
end;

procedure TIdleSpaceWar.UpdateGameState();
{$X+ J+}
const
  MAX_HIT_FX_TIMEOUT = 4; // frames 30fps - 0.5sec
begin
  if HitFXTimeout > 0 then
  begin
    HitFXTimeout := HitFXTimeout + 1;

    Caption := IntToStr(HitFXTimeout);

    if HitFXTimeout > MAX_HIT_FX_TIMEOUT then
    begin
      HitFXTimeout := 0;
      EnemyHitFX.Visible := False;
      EnemyGlowEffect.Enabled := False;
      PlayerLaserLine.Visible := False;
    end;
  end;

  for var anBullet in FActiveBulletList do
  begin
    anBullet.Update();
    anBullet.CheckHitEnemy(FEnemyShip);
  end;

  //TODO: Find delta time of previous frame with precision timer
  //TODO: Use delta time to smoother movement of bullet
end;

procedure TIdleSpaceWar.MoneyPathAnimation1Finish(Sender: TObject);
begin
  MoneyCircle.Visible := False;
end;

initialization

// enable the GPU on Windows
{$IFDEF MSWINDOWS}
  FMX.Types.GlobalUseGPUCanvas := true;
{$ENDIF}
  RegisterRenderingSetup;
  Randomize;

end.

