program isw;

uses
  System.StartUpCopy,
  FMX.Forms,
  game in 'game.pas' {IdleSpaceWar},
  enemyShip in 'src\enemyShip.pas',
  bullet in 'src\bullet.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TIdleSpaceWar, IdleSpaceWar);
  Application.Run;
end.
